<!DOCTYPE html>
<html>

<head>
  <!-- Basic -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <!-- Mobile Metas -->
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <!-- Site Metas -->
  <meta name="keywords" content="" />
  <meta name="description" content="" />
  <meta name="author" content="" />
  <link rel="shortcut icon" href="images/favicon.png" type="">

  <title> No Name Restaurant </title>

  <!-- bootstrap core css -->
  <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">

  <!--owl slider stylesheet -->
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" />
  <!-- nice select  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/css/nice-select.min.css" integrity="sha512-CruCP+TD3yXzlvvijET8wV5WxxEh5H8P4cmz0RFbKK6FlZ2sYl3AEsKlLPHbniXKSrDdFewhbmBK5skbdsASbQ==" crossorigin="anonymous" />
  <!-- font awesome style -->
  <link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}" />

  <!-- Custom styles for this template -->
  <link rel="stylesheet" href="{{ asset('css/acceuil.css') }}">
  <!-- responsive style -->
  <link rel="stylesheet" href="{{ asset('css/responsive.css') }}" />

  

</head>

<body>

  <div class="hero_area">
    <div class="bg-box">
      <img src="images/affiche.jpg" alt="">
    </div>
    <!-- header section strats -->
    <header class="header_section">
      <div class="container">
        <nav class="navbar navbar-expand-lg custom_nav-container ">
          <a class="navbar-brand" href="index.html">
            <span>
              No Name Restaurant
            </span>
          </a>

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class=""> </span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav  mx-auto ">
              <li class="nav-item active">
                <a class="nav-link" href="index.html">Acceuil <span class="sr-only">(current)</span></a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#food_section layout_padding-bottom">Menu</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#about_section layout_padding">A propos</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#book_section layout_padding">Reservations</a>
              </li>
            </ul>
       
          </div>
        </nav>
      </div>
    </header>
    <!-- end header section -->
    <!-- slider section -->
    <section class="slider_section ">
      <div id="customCarousel1" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          <div class="carousel-item active">
            <div class="container ">
              <div class="row">
                <div class="col-md-7 col-lg-6 ">
                  <div class="detail-box">
                    <h1>
                    Restaurant Africain
                    </h1>
               
                    <div class="btn-box">
                      <a href="" class="btn1">
                       Plus de details
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item ">
            <div class="container ">
              <div class="row">
                <div class="col-md-7 col-lg-6 ">
                  <div class="detail-box">
                    <h1>
                    Met en avant la gastromie africaine 
                    </h1>
                    <div class="btn-box">
                      <a href="" class="btn1">
                       Plus de details
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="container ">
              <div class="row">
                <div class="col-md-7 col-lg-6 ">
                  <div class="detail-box">
                    <h1>
                    Vous satisfaire est notre priorité
                    </h1>
                    <div class="btn-box">
                      <a href="" class="btn1">
                       Plus de details
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="container">
          <ol class="carousel-indicators">
            <li data-target="#customCarousel1" data-slide-to="0" class="active"></li>
            <li data-target="#customCarousel1" data-slide-to="1"></li>
            <li data-target="#customCarousel1" data-slide-to="2"></li>
          </ol>
        </div>
      </div>

    </section>
    <!-- end slider section -->
  </div>

  <!-- offer section -->

  <section class="offer_section layout_padding-bottom" id="food_section layout_padding-bottom">
    <div class="offer_container">
      <div class="container ">
        <div class="row">
          <div class="col-md-6  ">
            <div class="box ">
              <div class="img-box">
                <img src="images/chicken.jpg" alt="">
              </div>
              <div class="detail-box">
                <h5>
                  Poulet DG
                </h5>
                <h6>
                  <span>6500</span> fcfa
                </h6>
              </div>
            </div>
          </div>
          <div class="col-md-6  ">
            <div class="box ">
              <div class="img-box">
                <img src="images/poisson.jpg" alt="">
              </div>
              <div class="detail-box">
                <h5>
                  Poisson
                </h5>
                <h6>
                  <span>2000</span> fcfa
                </h6>
          
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end offer section -->

  <!-- food section -->

  <section class="food_section layout_padding-bottom">
    <div class="container">
      <div class="heading_container heading_center">
        <h2>
          Notre menu
        </h2>
      </div>

      <ul class="filters_menu">
        <li class="active" data-filter="*">Tout</li>
        <li  data-filter=".boissons_chaudes">Boissons chaudes</li>
        <li data-filter=".petitdej">Petit dejeuner</li>
        <li data-filter=".Crudités">Crudités</li>
        <li data-filter=".rizsenegalais">Riz senegalais</li>
      </ul>

      <div class="filters-content">
        <div class="row grid">
          <div class="col-sm-6 col-lg-4 all boissons_chaudes">
            <div class="box">
              <div>
                <div class="detail-box">
                  <h5>
                    Chaye au lait
                  </h5>
                  <p>
                    Boissons chaudes
                  </p>
                  <div class="options">
                    <h6>
                      200 fcfa
                    </h6>
               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all boissons_chaudes">
            <div class="box">
              <div>
                <div class="detail-box">
                  <h5>
                    The, Lipton, Nescafe, Chocolat
                  </h5>
                  <p>
                   Boissons chaudes
                  </p>
                  <div class="options">
                    <h6>
                      200 fcfa
                    </h6>
                 
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all boissons_chaudes">
            <div class="box">
              <div>
                <div class="detail-box">
                  <h5>
                    Chaye au gingembre
                  </h5>
                  <p>
                    Boissons chaudes
                  </p>
                  <div class="options">
                    <h6>
                      300 fcfa
                    </h6>
                
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all boissons_chaudes">
            <div class="box">
              <div>
                <div class="detail-box">
                  <h5>
                 Chaye au miel blanc
                  </h5>
                  <p>
                    Boissons chaudes
                  </p>
                  <div class="options">
                    <h6>
                      500 fcfa
                    </h6>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all boissons_chaudes">
            <div class="box">
              <div>
                <div class="detail-box">
                  <h5>
                    Cafe au lait
                  </h5>
                  <p>
                    Boissons chaudes
                  </p>
                  <div class="options">
                    <h6>
                      250 fcfa
                    </h6>
               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all petitdej">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f1.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                  Bouillon de pattes de Boeuf
                  </h5>
                  <p>
                    Petit Dejeuner
                  </p>
                  <div class="options">
                    <h6>
                      1500 fcfa
                    </h6>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all petitdej">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f1.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                    Jaret de Boeuf
                  </h5>
                  <p>
                    Petit Dejeuner
                  </p>
                  <div class="options">
                    <h6>
                      1500 fcfa
                    </h6>
            
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all petitdej">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f1.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                    Tripes de Boeuf
                  </h5>
                  <p>
                    Petit Dejeuner
                  </p>
                  <div class="options">
                    <h6>
                     1500 fcfa
                    </h6>
                    
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all Crudités">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f6.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                    Salade composé
                  </h5>
                  <p>
                    Crudité
                  </p>
                  <div class="options">
                    <h6>
                      1000 fcfa
                    </h6>
                
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all Crudités">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f6.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                  Omelette garnie avec boisson chaude au choix
                  </h5>
                  <p>
                    Crudité
                  </p>
                  <div class="options">
                    <h6>
                      1500 fcfa
                    </h6>
                   
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all rizsenegalais">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f1.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                  Riz senegalais à la viande
                  </h5>
                  <p>
                  Riz senegalais
                  </p>
                  <div class="options">
                    <h6>
                     1500 fcfa
                    </h6>
               
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all rizsenegalais">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f1.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                  Riz senegalais au poisson
                  </h5>
                  <p>
                  Riz senegalais
                  </p>
                  <div class="options">
                    <h6>
                      2000 fcfa
                    </h6>
             
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-lg-4 all rizsenegalais">
            <div class="box">
              <div>
                <div class="img-box">
                  <img src="images/f1.png" alt="">
                </div>
                <div class="detail-box">
                  <h5>
                  Riz senegalais au poulet

                  </h5>
                  <p>
                  Riz senegalais
                  </p>
                  <div class="options">
                    <h6>
                      2500 fcfa
                    </h6>
                 
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="btn-box">
        <a href="">
         Voir plus
        </a>
      </div>
    </div>
  </section>

  <!-- end food section -->

  <!-- about section -->

  <section class="about_section layout_padding" id="about_section layout_padding">
    <div class="container  ">

      <div class="row">
        <div class="col-md-6 ">
          <div class="img-box">
            <img src="images/affiche.jpg" alt="">
          </div>
        </div>
        <div class="col-md-6">
          <div class="detail-box">
            <div class="heading_container">
              <h2>
                Nous sommes le restaurant que vous recherchiez
              </h2>
            </div>
            <p>
             Notre préoccupation principale est de vous satisfaire. de s'assurer que chacun d'entre vous puisse trouver le repas qui le convient.
            </p>
            <a href="">
              voir plus
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end about section -->

  <!-- book section -->
  <section class="book_section layout_padding" id="book_section layout_padding">
    <div class="container">
      <div class="heading_container">
        <h2>
          Livraisons à domicile ou Services traiteurs
        </h2>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="form_container">
            <form action="">
              <div>
                <input type="text" class="form-control" placeholder="Nom" required/>
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Numéro de telephone" required/>
              </div>
              <div>
                <input type="email" class="form-control" placeholder="Adresse mail"required/>
              </div>
              <div>
                <select class="form-control nice-select wide" required>
                  <option value="" disabled selected>
                    Pour combien de personnes?
                  </option>
                  <option value="">
                    2
                  </option>
                  <option value="">
                    3
                  </option>
                  <option value="">
                    4
                  </option>
                  <option value="">
                    5
                  </option>
                </select>
              </div>
              <div>
                <input type="date" class="form-control">
              </div>
              <div class="btn_box">
                <button>
                 Effectuer
                </button>
              </div>
            </form>
          </div>
        </div>
   
        <div class="col-md-6">
      
        </div>
      </div>
    </div>
  </section>
  <!-- end book section -->

  <!-- client section -->

  <section class="client_section layout_padding-bottom">
    <div class="container">
      <div class="heading_container heading_center psudo_white_primary mb_45">
        <h2>
          Que dise nos clients
        </h2>
      </div>
      <div class="carousel-wrap row ">
        <div class="owl-carousel client_owl-carousel">
          <div class="item">
            <div class="box">
              <div class="detail-box">
                <p>
                 J'ai vraiment apprecié la qualité de service de ce restaurant.
                </p>
                <h6>
                  Vinicius
                </h6>
              </div>
              <div class="img-box">
              <img src="images/li1.jpeg" alt="" class="box-img">
              </div>
            </div>
          </div>
          <div class="item">
            <div class="box">
              <div class="detail-box">
                <p>
                  Les repas sont vraiment de bonne qualité. ainsi que le personnel.
                </p>
                <h6>
                 Aguero
                </h6>
              </div>
              <div class="img-box">
                <img src="images/li1.jpeg" alt="" class="box-img">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  <!-- end client section -->

  <!-- footer section -->
  <footer class="footer_section">
    <div class="container">
      <div class="row">
        <div class="col-md-4 footer-col">
          <div class="footer_contact">
            <h4>
              Contactez nous
            </h4>
            <div class="contact_link_box">
              <a href="">
                <i class="fa fa-map-marker" aria-hidden="true"></i>
                <span>
                  Yaoundé, Polyclinique Tsinga
                </span>
              </a>
              <a href="">
                <i class="fa fa-phone" aria-hidden="true"></i>
                <span>
                 697 16 20 90/ 679 94 36 63
                </span>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-col">
          <div class="footer_detail">
            <a href="" class="footer-logo">
             No name restaurant
            </a>
            <p>
             Restaurant Africain, mettant en avant la gastronomie africaine et ses ses saveurs
            </p>
            <div class="footer_social">
              <a href="">
                <i class="fa fa-facebook" aria-hidden="true"></i>
              </a>
              <a href="">
                <i class="fa fa-twitter" aria-hidden="true"></i>
              </a>
              <a href="">
                <i class="fa fa-linkedin" aria-hidden="true"></i>
              </a>
              <a href="">
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </a>
              <a href="">
                <i class="fa fa-pinterest" aria-hidden="true"></i>
              </a>
            </div>
          </div>
        </div>
        <div class="col-md-4 footer-col">
          <h4>
           Heure d'ouverture
          </h4>
          <p>
            Chaque jour
          </p>
          <p>
            08H à 21H
          </p>
        </div>
      </div>
      <div class="footer-info">
        <p>
          &copy; <span id="displayYear"></span> Tout droit reservé par
          <a href="https://html.design/">No name Restaurant</a><br><br>
          &copy; <span id="displayYear"></span> Distribué par 
          <a href="https://themewagon.com/" target="_blank">No name Restaurant</a>
        </p>
      </div>
    </div>
  </footer>
  <!-- footer section -->

  <!-- jQery -->
  <script src="{{ asset('js/jquery-3.4.1.min.js') }}"></script>
  <!-- popper js -->
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <!-- bootstrap js -->
  <script src="{{ asset('js/bootstrap.js') }}"></script>
  <!-- owl slider -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js">
  </script>
  <!-- isotope js -->
  <script src="https://unpkg.com/isotope-layout@3.0.4/dist/isotope.pkgd.min.js"></script>
  <!-- nice select -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-nice-select/1.1.0/js/jquery.nice-select.min.js"></script>
  <!-- custom js -->
  <script src="{{ asset('js/custom.js') }}"></script>
  <!-- Google Map -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap">
  </script>
  <!-- End Google Map -->

</body>

</html>